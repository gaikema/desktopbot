package com.mattgaikema.desktopbot;

import com.mashape.unirest.http.exceptions.UnirestException;
import com.mattgaikema.desktopbot.face.Face;
import com.mattgaikema.desktopbot.gaikbot.GaikBotConnector;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.layout.Pane;

/**
 *
 * @author mgaik
 */
public class FXMLController implements Initializable {
    
    @FXML
    private Label label;
    @FXML
    private Button button;
    @FXML
    private Button send;
    @FXML
    private TextArea textEntry;
    @FXML
    private ListView<String> messageContainer;
    private ObservableList<String> messages = FXCollections.observableArrayList();
	private GaikBotConnector gaikbot = new GaikBotConnector();
	@FXML
	private Pane interfacePane;
	@FXML
	private Canvas canvas;
	private Face face = new Face(500);
    
    @FXML
    private void handleButtonAction(ActionEvent event) {
        System.out.println("You clicked me!");
        label.setText("Hello World!");
    }
    
    @FXML
    private void sendButtonPressed(ActionEvent event) {		
		try {
		String content = textEntry.getText();
		if (content.isEmpty() || content == null)
			return;
		textEntry.clear();

        // Let's test this sucker out.
        //System.out.println(content);
        
        // Add message to list.
        messages.add("YOU: " + content);
        messageContainer.setItems(messages);
		
		// Send message to GaikBot.
		gaikbot.sendMessage(content);
		
		// Get and display response.
		String response = gaikbot.getResponse();
		messages.add("GAIKBOT: " + response);
		messageContainer.setItems(messages);
		}
		catch(UnirestException e) {
			messages.add("There was a server error.");
			messageContainer.setItems(messages);
		}
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
		face.draw(canvas.getGraphicsContext2D());
    }    
    
}

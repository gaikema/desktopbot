/*
* http://stackoverflow.com/a/36610533/5415895
*/
package com.mattgaikema.desktopbot.gaikbot;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Functions for connecting to the GaikBot server
 * and having a conversation with it.
 * @author mgaik
 */
public class GaikBotConnector {

	private String conversationID;
	private String token;
	
	/**
	 * Gets a token needed for using GaikBot's API.
	 * http://docs.botframework.com/sdkreference/restapi-directline/#!/Conversations/Conversations_NewConversation
	 * @throws UnirestException Bad stuff happens.
	 */
	public GaikBotConnector() {
		try {
			HttpResponse<String> response = Unirest.post("https://directline.botframework.com/api/conversations")
				.header("authorization", "BotConnector rZDCJBsdRpM.cwA.JYI.bAmr3_g2Jo4Ot87fZSc51NuFPmvroMPl9LpV1Uv9ZMQ")
				.header("cache-control", "no-cache")
				.header("postman-token", "1001791f-ac07-6589-40aa-a0420b698811")
				.asString();
			//String json = response.getBody();
			JSONObject json = new JSONObject(response.getBody());
			// Set token and conversationID from response.
			token = json.getString("token");
			conversationID = json.getString("conversationId");
		} catch (UnirestException ex) {
			Logger.getLogger(GaikBotConnector.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	
	/**
	 * POSTs a message to GaikBot via the direct line API.
	 * @param text The text of the message to send.
	 * @throws UnirestException Bad stuff.
	 */
	public void sendMessage(String text) throws UnirestException {
		HttpResponse<String> response = Unirest.post("https://directline.botframework.com/api/conversations/" + conversationID + "/messages")
			.header("authorization", "BotConnector rZDCJBsdRpM.cwA.JYI.bAmr3_g2Jo4Ot87fZSc51NuFPmvroMPl9LpV1Uv9ZMQ")
			.header("content-type", "application/json")
			.header("cache-control", "no-cache")
			.header("postman-token", "1af58b7a-5990-486d-f407-60684da24d55")
			.body("{\n    \"text\": \"" + text + "\"\n}")
			.asString();
	}
	
	/**
	 * Gets recent messages in a conversation.
	 * @return The response as a JSON string.
	 * @throws UnirestException bad stuff
	 */
	private String getMessages() throws UnirestException {
		HttpResponse<String> response = Unirest.get("https://directline.botframework.com/api/conversations/" + conversationID + "/messages")
			.header("authorization", "BotConnector rZDCJBsdRpM.cwA.JYI.bAmr3_g2Jo4Ot87fZSc51NuFPmvroMPl9LpV1Uv9ZMQ")
			.header("cache-control", "no-cache")
			.header("postman-token", "59ca67d1-701d-2537-184a-a1a47acf9174")
			.asString();
		
		return response.getBody();
	}
	
	/**
	 * Gets the response message.
	 * @return The most recent response.
	 * @throws UnirestException bad stuff
	 */
	public String getResponse() throws UnirestException {
		JSONObject json = new JSONObject(getMessages());
		JSONArray messageArray = json.getJSONArray("messages");
		// This is a hash map with elements of the message.
		JSONObject messageJson = messageArray.getJSONObject(0);
		
		// http://docs.oracle.com/javaee/7/api/javax/json/JsonObject.html#getJsonArray-java.lang.String-
		
		// Not done.
		return "q";
	}
	
}

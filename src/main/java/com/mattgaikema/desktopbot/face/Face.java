/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mattgaikema.desktopbot.face;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;

/**
 * Class for drawing and manipulating the face.
 * @author mgaik
 */
public class Face {
	
	// https://jaxenter.com/tutorial-a-glimpse-at-javafxs-canvas-api-105696.html
	
	private int radius;
	
	public Face(int r) {
		radius = r;
	}
	
	public void draw(GraphicsContext gc) {
		gc.setFill(Color.RED);
		gc.fillOval(100,100,radius/2,radius/2);
	}
}
